<br/>
<p align="center">
  <h3 align="center">ZennoHelper</h3>

  <p align="center">
    Библиотека классов и методов, которые предназначены для упрощённой разработки шаблонов на ZennoPoster.
    <br/>
    <br/>
    <a href="https://github.com/demiddima/ZennoHelperLib"><strong>Explore the docs »</strong></a>
    <br/>
    <br/>
    <a href="https://github.com/demiddima/ZennoHelperLib">View Demo</a>
    .
    <a href="https://github.com/demiddima/ZennoHelperLib/issues">Report Bug</a>
    .
    <a href="https://github.com/demiddima/ZennoHelperLib/issues">Request Feature</a>
  </p>
</p>

![Downloads](https://img.shields.io/github/downloads/demiddima/ZennoHelperLib/total) ![Contributors](https://img.shields.io/github/contributors/demiddima/ZennoHelperLib?color=dark-green) ![Issues](https://img.shields.io/github/issues/demiddima/ZennoHelperLib) ![License](https://img.shields.io/github/license/demiddima/ZennoHelperLib) 

## Содержание

* [О содержание библиотеки](#о-содержании-библиотеки)
  * [Class Log](#class-log)
    
## Особенности библиотеки

В данном разделе будут описывать некоторые особенности внутренностей библиотеки.

### Class Log

Класс для вывода лога, который будет использоваться на протяжении написания всей библиотеки. Для отображения сообщений в лог был выбран встроенный метод из библиотеки самого ZennoPoster - 'project.SendToLog()', особенностью которого является выбор типа лога(ошибка, предупреждение и информация) и цвета заливки. Данный метод предназначен для того, чтобы при написании кода избежать логирования было сложнее и сделать эту процедуру по-умолчанию обязательной.

